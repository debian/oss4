// HalPlayMixNew.h: interface for the CHalPlayMixNew class.
//
//////////////////////////////////////////////////////////////////////

#ifndef _HALPLAYMIXNEW_H
#define _HALPLAYMIXNEW_H

#include "Hal.h"
#include "LynxTWO.h"

enum
{
  MIXVAL_PMIXSRCE_RECORD0L = 0,
  MIXVAL_PMIXSRCE_RECORD0R,
  MIXVAL_PMIXSRCE_RECORD1L,
  MIXVAL_PMIXSRCE_RECORD1R,
  MIXVAL_PMIXSRCE_RECORD2L,
  MIXVAL_PMIXSRCE_RECORD2R,
  MIXVAL_PMIXSRCE_RECORD3L,
  MIXVAL_PMIXSRCE_RECORD3R,
  MIXVAL_PMIXSRCE_RECORD4L,
  MIXVAL_PMIXSRCE_RECORD4R,
  MIXVAL_PMIXSRCE_RECORD5L,
  MIXVAL_PMIXSRCE_RECORD5R,
  MIXVAL_PMIXSRCE_RECORD6L,
  MIXVAL_PMIXSRCE_RECORD6R,
  MIXVAL_PMIXSRCE_RECORD7L,
  MIXVAL_PMIXSRCE_RECORD7R,
  MIXVAL_PMIXSRCE_RECORD8L,
  MIXVAL_PMIXSRCE_RECORD8R,
  MIXVAL_PMIXSRCE_RECORD9L,
  MIXVAL_PMIXSRCE_RECORD9R,
  MIXVAL_PMIXSRCE_RECORD10L,
  MIXVAL_PMIXSRCE_RECORD10R,
  MIXVAL_PMIXSRCE_RECORD11L,
  MIXVAL_PMIXSRCE_RECORD11R,
  MIXVAL_PMIXSRCE_RECORD12L,
  MIXVAL_PMIXSRCE_RECORD12R,
  MIXVAL_PMIXSRCE_RECORD13L,
  MIXVAL_PMIXSRCE_RECORD13R,
  MIXVAL_PMIXSRCE_RECORD14L,
  MIXVAL_PMIXSRCE_RECORD14R,
  MIXVAL_PMIXSRCE_RECORD15L,
  MIXVAL_PMIXSRCE_RECORD15R,
  MIXVAL_PMIXSRCE_PLAY0L,
  MIXVAL_PMIXSRCE_PLAY0R,
  MIXVAL_PMIXSRCE_PLAY1L,
  MIXVAL_PMIXSRCE_PLAY1R,
  MIXVAL_PMIXSRCE_PLAY2L,
  MIXVAL_PMIXSRCE_PLAY2R,
  MIXVAL_PMIXSRCE_PLAY3L,
  MIXVAL_PMIXSRCE_PLAY3R,
  MIXVAL_PMIXSRCE_PLAY4L,
  MIXVAL_PMIXSRCE_PLAY4R,
  MIXVAL_PMIXSRCE_PLAY5L,
  MIXVAL_PMIXSRCE_PLAY5R,
  MIXVAL_PMIXSRCE_PLAY6L,
  MIXVAL_PMIXSRCE_PLAY6R,
  MIXVAL_PMIXSRCE_PLAY7L,
  MIXVAL_PMIXSRCE_PLAY7R,
  MIXVAL_PMIXSRCE_PLAY8L,
  MIXVAL_PMIXSRCE_PLAY8R,
  MIXVAL_PMIXSRCE_PLAY9L,
  MIXVAL_PMIXSRCE_PLAY9R,
  MIXVAL_PMIXSRCE_PLAY10L,
  MIXVAL_PMIXSRCE_PLAY10R,
  MIXVAL_PMIXSRCE_PLAY11L,
  MIXVAL_PMIXSRCE_PLAY11R,
  MIXVAL_PMIXSRCE_PLAY12L,
  MIXVAL_PMIXSRCE_PLAY12R,
  MIXVAL_PMIXSRCE_PLAY13L,
  MIXVAL_PMIXSRCE_PLAY13R,
  MIXVAL_PMIXSRCE_PLAY14L,
  MIXVAL_PMIXSRCE_PLAY14R,
  MIXVAL_PMIXSRCE_PLAY15L,
  MIXVAL_PMIXSRCE_PLAY15R
};

class CHalPlayMixNew
{
public:
  CHalPlayMixNew ()
  {
  }
   ~CHalPlayMixNew ()
  {
  }

  USHORT Open (PHALADAPTER pHalAdapter, USHORT usDstLine,
	       PPLAYMIXCTL_LO pPlayMixCtlLO, PPLAYMIXCTL_HI pPlayMixCtlHI,
	       PULONG pPlayMixStatus);

  ULONG GetVolume ()
  {
    return ((ULONG) m_ucMasterVolume);
  }
  void SetVolume (ULONG ulVolume);
  ULONG GetVolume (USHORT usLine);
  void SetVolume (USHORT usLine, ULONG ulVolume);
  BOOLEAN GetMute ()
  {
    return (m_bMasterMute);
  }
  void SetMute (BOOLEAN bMute);
  ULONG GetLevel ();
  void ResetLevel ();
  ULONG GetOverload ();
  void ResetOverload ();
  BOOLEAN GetDither ()
  {
    return (m_bDither);
  }
  void SetDither (BOOLEAN bDither);

  BOOLEAN GetMute (USHORT usLine);
  void SetMute (USHORT usLine, BOOLEAN bMute);
  ULONG GetSource (USHORT usLine);
  void SetSource (USHORT usLine, ULONG ulSource);

  //USHORT        GetFirstAvailableConnection( PUSHORT pusLine );
  //USHORT        SetConnection( USHORT usLine, BOOLEAN bConnect );

  USHORT SetDefaults (BOOLEAN bDriverLoading);
  USHORT SetMixerControl (USHORT usSrcLine, USHORT usControl, ULONG ulValue);
  USHORT GetMixerControl (USHORT usSrcLine, USHORT usControl,
			  PULONG pulValue);

private:
  void UpdateVolume (SHORT usLine);
  USHORT ConvertLine (USHORT usLine);

  CHalRegister m_RegMixControl[NUM_PMIXNEW_LINES];
  CHalRegister m_RegMixStatus;

  PHALADAPTER m_pHalAdapter;
  PHALMIXER m_pHalMixer;
  USHORT m_usDstLine;

  BYTE m_aucMonitorVolume[NUM_PMIXNEW_LINES * 2];
  BYTE m_aucMonitorSource[NUM_PMIXNEW_LINES * 2];
  BOOLEAN m_abMonitorMute[NUM_PMIXNEW_LINES * 2];

  BYTE m_ucMasterVolume;
  BOOLEAN m_bMasterMute;

  BOOLEAN m_bDither;
  ULONG m_ulOverloadCount;
};

#endif // _HALPLAYMIX_H
