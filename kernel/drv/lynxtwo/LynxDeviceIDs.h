/****************************************************************************
 LynxDeviceIDs.h

 Description:	LynxTWO Interface Header File

 Created: David A. Hoatson, April 2009
	
 Copyright � 2009 Lynx Studio Technology, Inc.

 This software contains the valuable TRADE SECRETS and CONFIDENTIAL INFORMATION 
 of Lynx Studio Technology, Inc. The software is protected under copyright 
 laws as an unpublished work of Lynx Studio Technology, Inc.  Notice is 
 for informational purposes only and does not imply publication.  The user 
 of this software may make copies of the software for use with products 
 manufactured by Lynx Studio Technology, Inc. or under license from 
 Lynx Studio Technology, Inc. and for no other use.

 THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
 PURPOSE.

 Environment: Windows 95/98/NT/2000/XP/Vista/OSX Kernel mode

 4 spaces per tab

 Revision History
 
 When      Who  Description
 --------- ---  ------------------------------------------------------------
****************************************************************************/
#ifndef _LYNXDEVICEIDS_H
#define _LYNXDEVICEIDS_H

#define LYNXDEVICE_LTMADI			0x0013
#define LYNXDEVICE_LTHD				0x0018
#define LYNXDEVICE_LTFW				0x0019
#define PCIDEVICE_LYNXTWO_A			0x0020
#define PCIDEVICE_LYNXTWO_B			0x0021
#define PCIDEVICE_LYNXTWO_C			0x0022
#define PCIDEVICE_LYNX_L22			0x0023
#define PCIDEVICE_LYNX_AES16		0x0024
#define PCIDEVICE_LYNX_AES16SRC		0x0025
#define LYNXDEVICE_AURORA16			0x0026
#define LYNXDEVICE_AURORA8			0x0027
#define PCIDEVICE_LYNX_AES16e		0x0028
#define PCIDEVICE_LYNX_AES16eSRC	0x0029
#define PCIDEVICE_LYNX_AES16e50		0x0030
#define LYNXDEVICE_HILO				0x0031
#define LYNXDEVICE_LTUSB			0x0032

#endif // _LYNXDEVICEIDS_H
