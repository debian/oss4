/*
 * Automatically generated file - do not edit.
 */
#include <oss_config.h>

#define DRIVER_NAME	lynxtwo
#define DRIVER_NICK	"lynxtwo"
#define DRIVER_STR_INFO	lynxtwo_str_info
#define DRIVER_ATTACH	lynxtwo_attach
#define DRIVER_DETACH	lynxtwo_detach
#define DRIVER_TYPE	DRV_PCI

extern int DRIVER_ATTACH (oss_device_t * ossdev);
extern int DRIVER_DETACH (oss_device_t * ossdev);
